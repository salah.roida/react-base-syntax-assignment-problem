import React from 'react';


const useroutput = ( props ) => {
    return (
        <div className="Useroutput">
            <p>Paragraph 1: {props.username1}</p>
            <p>Paragraph 2: {props.username2}</p>
        </div>
    )
};

export default useroutput;