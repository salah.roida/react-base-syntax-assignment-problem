import React, { Component } from 'react';
import './App.css';

import Useroutput from './UserOutput/Useroutput';
import Userinput from './UserInput/Userinput';

class App extends Component {
    state = {
   usernames : [
        'Ane',
        'Chien',
       'Grand ane',
       'Cochon',
       
   ]
  }
userNameHandler = (event) => {
    // console.log('Was clicked!');
    // DON'T DO THIS: this.state.persons[0].name = 'Maximilian';
    this.setState( {
      usernames : [
        'Barhouch',
        event.target.value,
       '3niyba',
       'Salopard',
       
   ]
    } )
  }
  render() {
      const style = {
      backgroundColor: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer'
    };
    return (
      <div className="App">
        <ol>
          <li>Create TWO new components: UserInput and UserOutput</li>
          <li>UserInput should hold an input element, UserOutput two paragraphs</li>
          <li>Output multiple UserOutput components in the App component (any paragraph texts of your choice)</li>
          <li>Pass a username (of your choice) to UserOutput via props and display it there</li>
          <li>Add state to the App component (=> the username) and pass the username to the UserOutput component</li>
          <li>Add a method to manipulate the state (=> an event-handler method)</li>
          <li>Pass the event-handler method reference to the UserInput component and bind it to the input-change event</li>
          <li>Ensure that the new input entered by the user overwrites the old username passed to UserOutput</li>
          <li>Add two-way-binding to your input (in UserInput) to also display the starting username</li>
          <li>Add styling of your choice to your components/ elements in the components - both with inline styles and stylesheets</li>
        </ol>
      
        <Userinput style={style} changed={this.userNameHandler} username={this.state.usernames[1]}/>
                <Useroutput username1="7imaroune" username2={this.state.usernames[0]} />
                <Useroutput username1="kalboune"  username2={this.state.usernames[1]}/>
                <Useroutput username1="kidaroune"  username2={this.state.usernames[2]}/>
                <Useroutput username1="7aloufoune"  username2={this.state.usernames[3]}/>
      </div>
      
    );
  }
}

export default App;
